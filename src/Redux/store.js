import { configureStore } from '@reduxjs/toolkit'
import timerReducer from './reducer'

export const store = configureStore({
    reducer:{
        timer:timerReducer
    }
})