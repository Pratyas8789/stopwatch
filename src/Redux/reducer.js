import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    hour: 0,
    minute: 0,
    second: 0,
    toogle: false,
}

const timerSlice = createSlice({
    name: "timer",
    initialState,
    reducers: {
        handleHour: (state) => {
            state.hour = state.hour + 1
            state.minute = 0
        },
        handleMinute: (state) => {
            state.minute = state.minute + 1
            state.second = 0
        },
        handleSecond: (state) => {
            state.second = state.second + 1
        },
        handleReset: (state) => {
            state.hour = 0,
            state.minute = 0,
            state.second = 0,
            state.toogle = false
        },
        handleToogle: (state) => {
            state.toogle = !state.toogle
        }

    }
})

export const { handleHour, handleMinute, handleSecond, handleReset, handleToogle } = timerSlice.actions

export default timerSlice.reducer