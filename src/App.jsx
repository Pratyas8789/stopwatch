import { useState } from 'react'
import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import Button from 'react-bootstrap/Button';
import { useDispatch, useSelector } from 'react-redux'
import { BsStopwatchFill } from 'react-icons/bs';
import { handleHour, handleMinute, handleReset, handleSecond, handleToogle } from './Redux/reducer';

function App() {
  const { hour, minute, second, toogle } = useSelector(store => store.timer)
  const dispatch = useDispatch()
  const [timeInterval, setTimeInterval] = useState();

  if (second == 60) {
    dispatch(handleMinute())
  }
  if (minute == 60) {
    dispatch(handleHour())
  }

  function handleStartClick(event) {
    dispatch(handleToogle())
    if (event.target.id === "reset") {
      dispatch(handleReset())
      clearInterval(timeInterval)
    }
    else {
      if (!toogle) {
        setTimeInterval(setInterval(() => {
          dispatch(handleSecond())
        }, 1000));
      }
      else {
        clearInterval(timeInterval)
      }
    }
  }

  return (
    <div className="app">
      <h1 className='text-light mb-5'> <BsStopwatchFill /> Stopwatch <BsStopwatchFill /></h1>
      <div className="mainContainer h-50 shadow-lg bg-white rounded-5 " >
        <div className="w-100 h-75 d-flex justify-content-around align-items-center ">
          <div className=" w-25 h-50  rounded-5">
            <div className=" w-100 h-25 mb-2 shadow-lg d-flex justify-content-center align-items-center rounded-5 text-dark" ><h5>Hour(s)</h5> </div>
            <div className=" w-100 h-75 shadow-lg rounded-5 d-flex justify-content-center align-items-center"  ><h1>{hour < 10 && "0"}{hour}</h1> </div>
          </div> <h1 className='mt-5' >:</h1>
          <div className=" w-25 h-50 ">
            <div className=" w-100 h-25 mb-2 shadow-lg d-flex justify-content-center align-items-center rounded-5 text-dark" > <h5>Minute(s)</h5> </div>
            <div className=" w-100 h-75 shadow-lg rounded-5 d-flex justify-content-center align-items-center"  ><h1>{minute < 10 && "0"}{minute}</h1> </div>
          </div><h1 className='mt-5' >:</h1>
          <div className=" w-25 h-50 ">
            <div className=" w-100 h-25 mb-2 shadow-lg d-flex justify-content-center align-items-center rounded-5 text-dark" > <h5>Second(s)</h5></div>
            <div className=" w-100 h-75 shadow-lg rounded-5 d-flex justify-content-center align-items-center"  ><h1>{second < 10 && "0"}{second}</h1></div>
          </div>
        </div>
        <div className="w-100 h-25 d-flex justify-content-around align-items-center">
          {!toogle ? <> <Button className='ps-5 pe-5 shadow-lg' variant="danger" onClick={handleStartClick} >Start</Button>{' '}</> : <><Button className='ps-5 pe-5 shadow-lg' variant="light" onClick={handleStartClick} >Pause</Button>{' '}</>}
          <Button className='ps-5 pe-5 shadow-lg' variant="light" id="reset" onClick={handleStartClick} >Reset</Button>{' '}
        </div>
      </div>
    </div>
  )
}

export default App
